<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModel extends Model
{
    protected $table = 'payments';
    protected $fillable = [
        'customerNumber', 'checkNumber','paymentDate','amount'
    ];
    public function customer()
  {
    return $this->belongsTo('App\CustomerModel','customerNumber');
  }
}
