<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLineModel extends Model
{
    protected $table = 'productlines';
   protected $primaryKey = 'productLine';
   protected $keyType = 'string';
    protected $fillable = [
        'productLine', 'textDescription','htmlDescription','image'
    ];

}
