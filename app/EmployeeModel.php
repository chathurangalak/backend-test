<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'employeeNumber';
    protected $fillable = [
        'employeeNumber', 'lastName','firstName','extension','email','officeCode','reportsTo','jobTitle'
    ];
    public function office()
{
    return $this->belongsTo('App\OfficeModel','officeCode');
}
public function employee()
{
return $this->belongsTo('App\EmployeeModel','employeeNumber');
}
}
