<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderModel;
use App\ProductModel;
use App\ProductLineModel;
class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        try{
            $order=OrderModel::find($id); //find the order
            if ( ! $order ) {
                return response()->json('Not Found')->setStatusCode(404);
            }
            $order_id=$order->orderNumber; //get order id
            $order_date=$order->orderDate; //get order date
            $status=$order->status; //get order status
            $bill_amount=0;
            $orderdetails=$order->orderdetails; //get orderdetails from orderdetails table
            $order_details_array=array();

            foreach($orderdetails as $od){  //create a orderdetails array and assign order details to that array in given format
                 $productcode=$od->productCode;
                $product=ProductModel::find($productcode);//find product table record
                $linetotal=$od->calculatelinetotal();  //get linetotal (this calculation is done in OrderDetailsModal class)
                $bill_amount+=$linetotal;
               $product_array=array('product'=>$product->productName,'product_line' => $product->productline->productLine,'unit_price'=>number_format($od->priceEach,2),'qty'=>$od->quantityOrdered,'line_total'=>number_format($linetotal,2));
            array_push($order_details_array,$product_array);
            }
            //create customer data arrray
            $customer=array('first_name'=>$order->customer->contactFirstName ,'last_name'=>$order->customer->contactLastName,'phone'=>$order->customer->phone,'country_code'=>$order->customer->country);
            //return responce
            return response()->json(['order_id'=>$order_id,'order_date'=>$order_date,'status'=>$status,'order_details'=>$order_details_array,'bill_amount'=>number_format($bill_amount,2),'customer'=>$customer])->setStatusCode(200);

        }catch (Exception $ex) { // Anything that went wrong
            return response()->setStatusCode(404);
        }
  }
}
