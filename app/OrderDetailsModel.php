<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailsModel extends Model
{
    protected $table = 'orderdetails';
    protected $fillable = [
        'orderNumber', 'productCode','quantityOrdered','priceEach','orderLineNumber'
    ];
    public function product()
  {
    return $this->belongsTo('App\ProductModel','productCode');
  }
  public function order()
  {
 return $this->belongsTo('App\OrderModel','orderNumber');
  }


  public function calculatelinetotal(){

    $priceeach=$this->attributes['priceEach'];
    $quantityOrdered=$this->attributes['quantityOrdered'];
   return $linetotal=$priceeach*$quantityOrdered;
  }
}
