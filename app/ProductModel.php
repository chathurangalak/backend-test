<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'productCode';
    protected $fillable = [
        'productCode', 'productName','productLine','productScale','productVendor','productDescription','quantityInStock','buyPrice','MSRP'
    ];
    public function productline()
  {
    return $this->belongsTo('App\ProductLineModel','productLine');
  }
}
