<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'orderNumber';
    protected $fillable = [
        'orderNumber', 'orderDate','requiredDate','shippedDate','status','comments','customerNumber'
    ];
    public function customer()
  {
    return $this->belongsTo('App\CustomerModel','customerNumber');
  }
  public function orderdetails()
  {
    return $this->hasMany('App\OrderDetailsModel','orderNumber','orderNumber');
  }
}
