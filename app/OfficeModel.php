<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeModel extends Model
{
    protected $table = 'offices';
    protected $fillable = [
        'officeCode', 'city','phone','addressLine1','addressLine2','state','country','postalCode','territory'
    ];


}
